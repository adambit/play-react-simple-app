
import React, { Component } from 'react'
import Client from "./Client";

class ClubList extends Component {

  constructor(props) {
    super(props);
    this.state = { list: [] };
  }

  async componentDidMount() {
    Client.getClubs(clubs => {
      this.setState({
        list: clubs
      });
    });
  }

  render () {


    const memberItems = this.state.list.map((club) =>
      <li>
        {club.name} members:
        <ul>
        {club.members.map((member) =>
          <li>{member}</li>
        )}
        </ul>
      </li>
    );

    return (
      <div align="left">
        <h3>Clubs:</h3>
        <ul>{memberItems}</ul>
      </div>
    )
  }
}

export default ClubList

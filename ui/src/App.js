import React, {Component} from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import Client from "./Client";

import './App.css';
import ClubList from './ClubList'
import CreateClub from './CreateClub'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {title: ''};
  }

  async componentDidMount() {
    Client.getSummary(summary => {
      this.setState({
        title: summary.content
      });
    });
  }

  render() {
    return (
      <Router>
        <div className="App">
          <h1>Welcome to {this.state.title}!</h1>

          <nav>
          <div align="left">
             <ul className="main-menu">
              <li> <Link to="/create"> CreateClub </Link> </li>
              <li> <Link to="/clubs"> Clubs </Link> </li>
             </ul>
          </div>
          </nav>

          <Route path="/clubs" component={ClubList}/>
          <Route path="/create" component={CreateClub}/>

        </div>
      </Router>
    );
  }
}
export default App;

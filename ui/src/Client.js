/* eslint-disable no-undef */
function getSummary(cb) {
  return fetch('/api/summary', {
    accept: "application/json"
  })
    .then(checkStatus)
    .then(parseJSON)
    .then(cb);
}

/* eslint-disable no-undef */
function getClubs(cb) {
  return fetch('/api/clubs', {
    accept: "application/json"
  })
  .then(checkStatus)
  .then(parseJSON)
  .then(cb)
}

function createClub(data) {
  return fetch('/api/clubs', {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: 'PUT',
    body: data
  })
  .then(checkStatus)
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(`HTTP Error ${response.statusText}`);
  error.status = response.statusText;
  error.response = response;
  console.log(error); // eslint-disable-line no-console
  throw error;
}

function parseJSON(response) {
  return response.json();
}

const Client = { getSummary, getClubs, createClub };
export default Client;

import React, { Component } from 'react'
import Client from "./Client";

class CreateClub extends Component {

  constructor() {
      super();
      this.state = {
        name: '',
        members: [{ name: '' }],
      };
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = {
      name: event.target.name.value,
      members: this.state.members.map((member) => member.name)
    };

    Client.createClub(JSON.stringify(data));

    event.target.reset();
    this.setState({
      name: '',
      members: [{ name: '' }],
    });
    this.forceUpdate();
  }

  handleMemberNameChange = (idx) => (evt) => {
    const newMembers = this.state.members.map((member, sidx) => {
    if (idx !== sidx) return member;
      return { ...member, name: evt.target.value };
    });

    this.setState({ members: newMembers });
  }

  handleAddMember = () => {
    this.setState({
      members: this.state.members.concat([{ name: '' }])
    });
  }

  handleRemoveMember = (idx) => () => {
    this.setState({
      members: this.state.members.filter((s, sidx) => idx !== sidx)
    });
  }

  render () {
    return (
            <form align="left" onSubmit={this.handleSubmit}>
              <h4>Enter Club name</h4>
              <input id="name" name="name" type="text" />


              <h4>Enter members</h4>

              {this.state.members.map((member, idx) => (
                <div className="member">
                  <input
                    type="text"
                    placeholder={`Member ${idx + 1}`}
                    value={member.name}
                    onChange={this.handleMemberNameChange(idx)}
                  />
                  <button type="button" onClick={this.handleRemoveMember(idx)} className="small">Remove</button>
                </div>
                      ))}
              <button type="button" onClick={this.handleAddMember} className="small">Add Member</button>

              <div><button>Create</button></div>
            </form>
    )
  }
}

export default CreateClub

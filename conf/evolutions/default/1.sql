# Clubs schema

# --- !Ups

create table club (
  id                        varchar(36) not null,
  name                      varchar(255) not null,
  constraint pk_club primary key (id)
);

create table member (
  id                        varchar(36) not null,
  club_id                   varchar(36) not null,
  name                      varchar(255) not null,
  constraint pk_member primary key (id)
);


alter table member add constraint fk_member_club foreign key (club_id) references club (id) on delete restrict on update restrict;
create index ix_member_club on member (club_id);

SET REFERENTIAL_INTEGRITY FALSE;

# --- !Downs

drop table if exists member;

drop table if exists club;
package controllers

import akka.stream.scaladsl.Source
import javax.inject._
import model.Club
import play.api.libs.json.Json
import play.api.mvc._
import service.ClubsService

@Singleton
class HomeController @Inject()(cc: ControllerComponents, clubsService: ClubsService) extends AbstractController(cc) {

  import JsonFormats._

  def getClubs = Action {
    Ok.chunked(Source.fromFuture(clubsService.getAll).map(clubs => Json.toJson(clubs)))
  }

  def saveClub = Action(parse.json) { request =>

    request.body.validate[Club].map { club =>
        Created.chunked(Source.fromFuture(clubsService.save(club)).map(_ => Json.toJson(club)))
    }.recoverTotal { e =>
      BadRequest("Invalid joson")
    }
  }

  def appSummary = Action {
    Ok(Json.obj("content" -> "Clubs site"))
  }

  object JsonFormats {
    import play.api.libs.json._
    import play.api.libs.functional.syntax._

    private val clubNamePath = JsPath \ "name"
    private val clubMembersPath = JsPath \ "members"

    implicit val clubReads: Reads[Club] = (
        clubNamePath.read[String] and
        clubMembersPath.read[List[String]]
    )(Club.apply _)

    implicit val clubWrites: Writes[Club] = (
        clubNamePath.write[String] and
        clubMembersPath.write[List[String]]
    )(unlift(Club.unapply))
  }
}

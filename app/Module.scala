import com.google.inject.AbstractModule
import com.google.inject.name.Names
import data.repository.{ClubRepository, MemberRepository}
import data.transaction.{ScalikeTransactionExecutor, TransactionExecutor}
import service.ClubsService

import scala.concurrent.ExecutionContext

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.

 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
class Module extends AbstractModule {

  override def configure(): Unit = {

    bind(classOf[ExecutionContext]).annotatedWith(Names.named("defaultEC"))
            .toInstance(scala.concurrent.ExecutionContext.Implicits.global)
    bind(classOf[ClubRepository]).asEagerSingleton()
    bind(classOf[MemberRepository]).asEagerSingleton()
    bind(classOf[TransactionExecutor]).toInstance(ScalikeTransactionExecutor)
    bind(classOf[ClubsService]).asEagerSingleton()
  }
}

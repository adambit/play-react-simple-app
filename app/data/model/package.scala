package data

import java.util.UUID

package object model {

  case class Club(
    id: UUID,
    name: String
  )

  case class Member(
    id: UUID,
    clubId: UUID,
    name: String
  )
}

package data.transaction

import scalikejdbc.{DB, DBSession}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

trait TransactionExecutor {
  def asyncTx[A](f: DBSession => A)(implicit executionContext: ExecutionContext): Future[A]
}

object ScalikeTransactionExecutor extends TransactionExecutor {
  override def asyncTx[A](f: DBSession => A)(implicit executionContext: ExecutionContext): Future[A] =
    DB.futureLocalTx(session => Future(f(session)))
}

// for testing purpose
class PassThroughTransactionExecutor(session: DBSession) extends TransactionExecutor {
  override def asyncTx[A](f: DBSession => A)(implicit executionContext: ExecutionContext): Future[A] =
    Try(f(session)) match {
      case Success(a) =>
        Future.successful(a)
      case Failure(t) =>
        Future.failed(t)
    }
}


package data.repository

import java.util.UUID

import data.model.Club
import scalikejdbc._

class ClubRepository {

  def save(club: Club)(implicit session: DBSession): Unit = {
    sql"""insert into club values (${club.id.toString}, ${club.name})"""
        .execute()
        .apply()
  }

  def findAll()(implicit session: DBSession = ReadOnlyAutoSession): List[Club] = {
    sql"""select * from club"""
        .map { rs =>
          Club(
            id = UUID.fromString(rs.string("id")),
            name = rs.string("name")
          )
        }
        .list()
        .apply()
  }
}

package data.repository

import java.util.UUID

import data.model.{Club, Member}
import scalikejdbc._


class MemberRepository {

  def save(members: List[Member])(implicit session: DBSession): Unit = {

    val batch = members.map(member => List(member.id, member.clubId, member.name))

    sql"""insert into member values (?, ?, ?)"""
        .batch(batch: _*)
        .apply()
  }

  def findAll()(implicit session: DBSession = ReadOnlyAutoSession): List[Member] = {
    sql"""select * from member"""
        .map { rs =>
          Member(
            id = UUID.fromString(rs.string("id")),
            clubId = UUID.fromString(rs.string("club_id")),
            name = rs.string("name")
          )
        }
        .list()
        .apply()
  }
}

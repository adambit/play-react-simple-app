package service

import java.util.UUID

import data.repository.{ClubRepository, MemberRepository}
import data.transaction.TransactionExecutor
import javax.inject.{Inject, Named}
import model.Club

import scala.concurrent.{ExecutionContext, Future}

class ClubsService @Inject() (
  clubRepository: ClubRepository,
  memberRepository: MemberRepository,
  transactionExecutor: TransactionExecutor
)(implicit @Named("defaultEC") executionContext: ExecutionContext) {

  def save(club: Club): Future[Unit] = {

    val clubData = data.model.Club(
      id = UUID.randomUUID(),
      name = club.name
    )

    val membersData = club.members.map { name =>
      data.model.Member(
        id = UUID.randomUUID(),
        clubId = clubData.id,
        name = name
      )
    }

    transactionExecutor.asyncTx { implicit session =>
      clubRepository.save(clubData)
      memberRepository.save(membersData)
    }
  }

  def getAll: Future[List[Club]] = for {
    membersGroupedByClubId <- Future(memberRepository
        .findAll()
        .groupBy(_.clubId))
    clubs <- Future {
      clubRepository
          .findAll()
          .map { club =>
            Club(
              name = club.name,
              members = membersGroupedByClubId.getOrElse(club.id, List.empty).map(_.name)
            )
          }
    }
  } yield {
    clubs
  }
}

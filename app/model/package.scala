package object model {

  case class Club(
    name: String,
    members: List[String]
  )
}
